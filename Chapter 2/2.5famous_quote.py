# Famous Quote: Find a quote from a famous person you admire. Print the
# quote and the name of its author. Your output should look something like the
# following, including the quotation marks:
# Albert Einstein once said, “A person who never made a mistake never tried
# anything new.”
first_name = "randy"
last_name = "smith"
famous_person = f"{first_name} {last_name}"
print(f"{famous_person.title()} once said, You miss 100% of the shots you never take!")
