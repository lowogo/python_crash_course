# Famous Quote 2: Repeat Exercise 2-5, but this time, represent the famous
# person’s name using a variable called famous_person. Then compose your
# message and represent it with a new variable called message. Print your
# message.
first_name = "randy"
last_name = "smith"
famous_person = f"{first_name} {last_name}"
message = "You miss 100% of the shots you never take!"
print(f"{famous_person.title()} once said, {message}")
