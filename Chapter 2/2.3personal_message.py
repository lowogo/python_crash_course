# Personal Message: Use a variable to represent a person’s name, and print a
# message to that person. Your message should be simple, such as, “Hello Eric,
# would you like to learn some Python today?”
first_name = "Randy"
print(f"Hello {first_name}, would you like to learn some Python today?")
