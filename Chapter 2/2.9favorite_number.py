# Favorite Number: Use a variable to represent your favorite number. Then,
# using that variable, create a message that reveals your favorite number. Print that
# message.
number = 6
message = "My favourite number is " + str(number) + "."
# + str(number) + , the two + required or code will fail
print(message)
