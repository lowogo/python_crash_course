# Stripping Names: Use a variable to represent a person’s name, and include
# some whitespace characters at the beginning and end of the name. Make sure
# you use each character combination, "\t" and "\n", at least once.
favorite_sport = " basketball "
print(favorite_sport)
print(favorite_sport.lstrip())
print(favorite_sport.rstrip())
print(favorite_sport.strip())
