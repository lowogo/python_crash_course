# Your Own List: Think of your favorite mode of transportation, such as a
# motorcycle or a car, and make a list that stores several examples. Use your list to
# print a series of statements about these items, such as “I would like to own a
# Honda motorcycle.”

motorbikes = ['Wave', 'Winner X', 'Future', 'Vision']
message = "I would like to own a Honda "
print(message + motorbikes[0] + " motorcycle.")
print(message + motorbikes[1] + " motorcycle.")
print(message + motorbikes[2] + " motorcycle.")
print(message + motorbikes[-1] + " motorcycle.")
